
$(document).ready(function(){

	"use strict";


// MOBILE NAV —————————————————————————————————————————————————————————————————————————
// —————————————————————————————————————————————————————————————————————————————————

	$('.hamburger').on('click', function() {

		$(this).find('.bar2, .bar3').toggleClass('visible'),
		$(this).find('.bar:not(.cross)').toggleClass('invisible');
		$('header nav').toggleClass('open');


	});

// VIMEO ———————————————————————————————————————————————————————————————————————————
// —————————————————————————————————————————————————————————————————————————————————
	var iframe = $('#video')[0],
    player = $f(iframe),
    playing = false, 



	playVideo = function(e) {
		var $frame = $('.video');
		var $vid = $frame.find('iframe');
		var $img = $frame.find('img');
		$vid.toggleClass('visible');
		$img.toggleClass('visible');
		if (playing) {
			player.api("pause");
		} else {
			player.api("play");
		}
		e.preventDefault();
		return;
	};

	$('.video').on('click', playVideo);






// SCROLLMAGIC —————————————————————————————————————————————————————————————————————
// —————————————————————————————————————————————————————————————————————————————————

	// UNIQUE SCROLLMAGIC CONTROLLER
	var controller = new ScrollMagic.Controller(),

	// MENU ANIMATIONS
	menuAnim = TweenLite.to("header ul li a", 0.2, {fontSize: "2.5rem", ease: Cubic.easeIn}),
	logoAnim = TweenLite.to("a.home", 0.2, {height: "50%"}),


	// PARALLAX LIST

	// Duration / height of scroll for each animation
		// Percentage refers to viewport height, i.e., total height you scoll while animating
	durations = [
		"250%",
		"200%",
		"200%",
		"180%",
		"200%",
		"100%"
	],
	// Parallax animations themselves
	parallax = [
		TweenLite.to("#p1", 1, {backgroundPosition: "center 125%", ease: Linear.easeNone}),
		TweenLite.to("#p2", 1, {backgroundPosition: "center 125%", ease: Linear.easeNone}),
		TweenLite.to("#p3", 1, {backgroundPosition: "center 125%", ease: Linear.easeNone}),
		TweenLite.to("#p4", 1, {backgroundPosition: "center 125%", ease: Linear.easeNone}),
		TweenLite.to("#p5", 1, {backgroundPosition: "center 190%", ease: Linear.easeNone}),
		TweenLite.to("#p6", 1, {backgroundPosition: "center 100%", ease: Linear.easeNone})
	],
	// FONTSIZE ANIMATION
	menu = new ScrollMagic.Scene({triggerHook: "onLeave", triggerElement: "body", offset: 70})
		.setTween(menuAnim)
		// .addIndicators({name: "navegação"}) <= debug info
		.addTo(controller),
	// LOGO REDUCTION ANIMATION
	logo = new ScrollMagic.Scene({triggerHook: "onLeave", triggerElement: "body", offset: 70})
		.setTween(logoAnim)
		.addTo(controller),
	// Dynamic IDs
	p = [],
	// Dynamic getting of heights (if not set)
	// heights = [],
	getHeight = function(arg) {
		var $altura = $(this).outerHeight();
		var $height = [];
		$height.push($altura);
		return $height[arg];
	},

	// ADD each PARALLAX SCENE DYNAMICALLY
	setElements = function() {
		$('.parallax').each(function(i) {
			var id = "p" + parseInt(i+1);
			p[i] = new ScrollMagic.Scene({triggerHook: "onEnter", triggerElement: "#" + id, duration: durations[i] })
				.setTween(parallax[i])
				// .addIndicators({name: "parallax" + parseInt(i+1)})  <= debug info
				.addTo(controller);
				console.log(id);
		});
	};

	//RUN DIZ SHYTE
	setElements();

	// SCROLLMAGIC FIX FOR „IE”
	if(navigator.userAgent.match(/Trident\/7\./)) {
        $('body').on("mousewheel", function () {
            console.log('hey');
            event.preventDefault();

            var wheelDelta = event.wheelDelta;

            var currentScrollPosition = window.pageYOffset;
            window.scrollTo(0, currentScrollPosition - wheelDelta);
        });

        $(window).on("scroll", function() {
		    controller.update(true);
		});
	}

// ACCORDEON —————————————————————————————————————————————————————————————————————
// —————————————————————————————————————————————————————————————————————————————————

	$('.accordion').accordion();

	
});
