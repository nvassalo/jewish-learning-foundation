<?php $page = 'what' ?>
<?php include 'header.php' ?>
	
	<div class="container-fluid parallax" style="background-image: url(assets/img/_parallax-1.jpg)" id="p1">
		<div class="container what">
			<div class="row">
				<article class="row md-8 md-push-2">
					<h1>Our story</h1>
				
					<p>the Jewish Learning Fellowship (JLF) is a 10-week experiential, conversational seminar for students looking to deepen their understanding of Judaism on their own terms. We’re interested in asking big questions. you know, the big stuff — like Who am I? What communities am I a part of? What is worth committing myself to, and why? And we don’t purport to have any of the big answers...certainly not for anyone else. JLF was cooked up in 2007 at the Bronfman Center for Jewish Student Life at Nyu. Since then, JLF has graduated over 1,400 fellows.</p>
					<p>Over the years, the JLF team would frequently receive really, really nice thank you notes from students at the end of the semester. they described their experience with adjectives like: Inclusive, Alive, Expansive, Warm, Exploratory, Stimulating, Welcoming, Intriguing, Non-Judgemental, Deep, Inspiring, and unforgettable. they said things that made us blush, like:
		“thank you thank you thank you JLF. you have been such a gift and pleasure.” Or, “I’m really happy I had the chance to be in this class! I wish I wasn’t graduating so I could do it again!” Or, “this class should really be taught to everyone – it is very special and rewarding.”</p>
					<p>Or, our favorite: “One of the best classes I have ever taken at Nyu.” So, we decided we must have something pretty good going. In 2015, we decided it was time to expand JLF to several new college campuses and sites around the country!
		We are proud to say that we make no claims about the “right” way to practice or not to practice Judaism. Our job is to help you explore the tradition in a safe space and find your own place, on your terms, in Judaism’s Great Conversation.</p>
				
				</article>
			</div>
		</div>
	</div>
	<div class="container-fluid parallax" style="background-image: url(assets/img/_parallax-2.jpg)"  id="p2">
		<div class="container what">
			<div class="row">

				<article class="row md-8 md-push-2">
					<h1>Our vision</h1>
					<p>We believe in the power of people and relationships, and we believe that Jewish texts can be multifaceted and intriguing. By the end of 10 weeks, we want fellows to feel like they have:</p>
					<ol>
						<li>
							Jewish friends.
							<p>Let’s be real: Jewish life doesn’t happen by yourself in your dorm room or apartment. It happens with other people. We want you to feel like you have a bigger Jewish social network at the end of JLF.</p>
						</li>
						<li>
							A Jewish community.
							<p>We want you to feel a part of something bigger than yourself, and we want you to feel like you have a community after JLF, if you want one. What we’re saying is, we’re going to invite you to get coffee.</p>
						</li>
						<li>
							...learned something interesting.
							the wisdom found in <p>Jewish texts, both ancient and contemporary, can be surprising, complicated, delightful, weird, mysterious, perplexing, and FuN. We’ll read these texts broadly and generously in JLF, and we count on fellows adding their unique voices and perspectives into our conversations.</p>
						</li>
					</ol>
				</article>
			</div>
		</div>
	</div>
	<div class="container-fluid parallax sub" style="background-image: url(assets/img/_parallax-3.jpg)"  id="p3">
		<div class="container what">
			<div class="row">

				<article class="row md-8 md-push-2" id="program">
					<h1>The program</h1>
					<p>JLF is a 10 week seminar. We’ll break bread together each week. We’re also going to invite you to coffee, spend a Shabbat together, and help you feel connected to next steps and alumni offerings after JLF ends. Fellows can earn a $300 stipend at the end of the program. here’s a peek at how all of this looks:</p>
				</article>
				<article class="row md-8 md-push-2" id="course">
					
					<h1>Course</h1>
					<h2>Course Descriptions and syllabi</h2>

					<ul class="accordion">
					    <li class="current">
						    <a href="#course-desc" class="accordeon-btn">Life’s Big Questions, or How to Get More out of college</a>
						    <div>
								<p>college is not only a time to meet new people or to learn the skills of
								a profession. It is also a time to explore some of the big questions that orient a life well-lived. Who am I? What communities am I a part of? Who am I responsible for and why? What is the difference between love, lust and intimacy? What is worth committing myself to and why? While these are universal questions, we believe they can be illuminated through the unique light of Jewish texts and traditions. In this class we will explore the big questions of life as refracted through the Jewish tradition.</p>
							</div>
					        <ul>
					            <li>
					            	<a href="#course-syllabus" class="sub-accordeon-btn">Syllabus</a>
							    	<div>
										<p>college is not only a time to meet new people or to learn the skills of
									a profession. It is also a time to explore some of the big questions that orient a life well-lived. Who am I? What communities am I a part of? Who am I responsible for and why? What is the difference between love, lust and intimacy? What is worth committing myself to and why? While these are universal questions, we believe they can be illuminated through the unique light of Jewish texts and traditions. In this class we will explore the big questions of life as refracted through the Jewish tradition.</p>
									</div>
					            </li>
					        </ul>
					    </li>
					    <li>
						    <a href="#second" class="accordeon-btn">Life’s Big Questions, Or How to Get More Out of College</a>
					    	<div>
								<p>But not a drill worth committing myself to and why? While these are universal questions, we believe they can be illuminated through the unique light of Jewish texts and traditions. In this class we will explore the big questions of life as refracted through the Jewish tradition.</p>
			                </div>
			            </li>
					</ul>
						
				</article>
			</div>
		</div>
	</div>
	<div class="container-fluid parallax sub" style="background-image: url(assets/img/_parallax-4.jpg)"  id="p4">
		<div class="container what">
			<div class="row">
				<article class="row md-8 md-push-2" id="coffee">
					<h1>Coffee date</h1>
					<img src="assets/img/_coffee-date.jpg" height="233" width="674" alt="">
					<p>A great teacher, Taylor swift, once said: “I really really really really really really like you.” Her elder, T-Pain, proclaimed “let me buy you a drank.” owing to the philosophies of these great minds, we want to get to know our fellows beyond the walls of our seminar — preferably over something caffeinated or made out of fro-yo. JLF educators and team members will invite fellows to coffee over the course of the semester to hear their stories, debrief their JLF experience, and to learn about their interests, ambitions, and passions.</p>	
				</article>
			</div>
		</div>
	</div>
	<div class="container-fluid parallax sub" style="background-image: url(assets/img/_parallax-1.jpg)"  id="p5">
		<div class="container what">
			<div class="row">
				<article class="row md-8 md-push-2" id="experiences">
					<h1>Shabbath experiences</h1>
					<img src="assets/img/_coffee-date.jpg" height="233" width="674" alt="">
					<p>A great teacher, Taylor swift, once said: “I really really really really really really like you.” Her elder, T-Pain, proclaimed “let me buy you a drank.” owing to the philosophies of these great minds, we want to get to know our fellows beyond the walls of our seminar — preferably over something caffeinated or made out of fro-yo. JLF educators and team members will invite fellows to coffee over the course of the semester to hear their stories, debrief their JLF experience, and to learn about their interests, ambitions, and passions.</p>	
				</article>
			</div>
		</div>
	</div>
	<div class="container-fluid parallax" style="background-image: url(assets/img/_parallax-2.jpg)" id="p6">
		<div class="container what">
			<div class="row">
				<article class="row md-8 md-push-2">
					<h1>What comes after</h1>
					<div>
						<p>Fellows will spend 1-2 Friday evenings together during JLF, either on campus or in the home of their JLF educator. This is a chance to share time with one another outside of our seminar, break bread together, and experience shabbat...whether it's our first time or our fiftieth.</p>
					</div>
				</article>
			</div>
		</div>
	</div>	


<?php include 'footer.php' ?>