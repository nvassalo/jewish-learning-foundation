		<footer class="container-fluid">
			<div class="pull-left">
				54 W 40th St, New York, NY 10018 <br/>
				212.998.4123
			</div>
			<div class="pull-right">
				<img src="assets/svg/hillel-logo.svg" alt="" height="16" width="79">
				<img src="assets/svg/ooi-logo.svg" alt=""> <br />
				info@jlfhillel.org <br/>
				Copyright © 2015 JLF. All rights reserved.
			</div>
		</footer>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js" defer></script>
	<script src="assets/js/jquery.accordion.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js" defer></script>
	
	<script src="assets/js/mobile-menu.js" defer></script>
	<script src="https://f.vimeocdn.com/js/froogaloop2.min.js" defer></script>
	<script src="assets/js/main.js" defer></script>
</html>