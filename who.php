<?php  $page = 'who'; ?>
<?php include 'header.php' ?>

<div class="container" id="who">
	<article class="row md-12" id="team">
		<header class="row xs-12 md-8 md-push-2">
			<h1>Our team</h1>
		</header>
		<section class="row md-12 middle">
			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_team_01.jpg" height="299" width="219" alt="">

				<div>
					<h2>Erica Frankel</h2>
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>

			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_team_02.jpg" height="299" width="219" alt="">
				<div>
					<h2>Dan Smokler</h2>
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>

			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_team_03.jpg" height="299" width="219" alt="">

				<div>
					<h2>Sam Cohen</h2>	
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>
		</section>
		
	</article>
	<article class="row md-12" id="partners">
		<header class="row md-8 md-push-2 xs-12">
			<h1>Our partners</h1>
		</header>
		<div class="middle row">
			<div class="column md-4 sm-6 xs-6">
				<div>
					<img src="assets/img/_partner.png" height="74" width="219" alt="">
				</div>
			</div>
			<div class="column md-4 sm-6 xs-6">
				<div>
					<img src="assets/img/_partner.png" height="74" width="219" alt="">
				</div>
			</div>
			<div class="column md-4 sm-6 xs-6">
				<div>
					<img src="assets/img/_partner.png" height="74" width="219" alt="">
				</div>
			</div>
			<div class="column md-4 sm-6 xs-6">
				<div>
					<img src="assets/img/_partner.png" height="74" width="219" alt="">
				</div>
			</div>
			<div class="column md-4 sm-6 xs-6">
				<div>
					<img src="assets/img/_partner.png" height="74" width="219" alt="">
				</div>
			</div>
		</div>
	</article>
	<article class="row md-8 md-push-2 xs-12 invert" id="join">
		<h1>Want to start a JLF on your campus?</h1>
		<p>Interested in participating in JLF, but don’t see your campus reflected here? We’d love to let your Hillel know that there’s interest. Please email us to voice your interest, and we’ll be in touch with our partners and colleagues at your university!</p>
		</p>
		<a href="#" id="contact">Contact us here</a>
	</article>
	<article class="row md-12" id="family">
		<header class="row md-8 md-push-2 xs-12">
			<h1>Our family</h1>
		</header>
		<section class="middle row">
			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_family_01.png" height="296" width="219" alt="">
				<div>
					
					<h2>Sam Cohen</h2>	
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>
			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_family_02.png" height="296" width="219" alt="">
				<div>
					
					<h2>Sam Cohen</h2>	
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>
			<div class="column md-4 sm-6 xs-12">
				<img src="assets/img/_family_03.png" height="296" width="219" alt="">
				<div>
					
					<h2>Sam Cohen</h2>	
					<p>Erica is the Director for Strategic Development for the Jewish Learning Fellowship at hillel’s Office of Innovation, as well as a modern dancer and performer in and around New york City.</p>
		<p>Erica earned a certificate in Experiential Jewish Education from yeshiva university and was selected for the inaugural cohorts of hillel International’s harrison LApID
		and Limmud Ny’s Community of Educators. She serves as Director
			of Engagement for National Choreography month and helped publish the Fy10 Dance Workforce Census as a member of the Dance/ NyC Junior Committee.</p>
		<p>Erica holds BFA and mA degrees in Dance from Nyu, where she was a four-time awardee of the Gallatin Jewish Studies Grant and recipient of the Give-A-violet Award, the university’s most prestigious professional recognition. She lives in harlem with her boyfriend and her cat.</p>
				</div>
			</div>
		</section>
		</article>
		<article class="row md-12" id="supporters">
			<header class="row md-8 md-push-2 clearfix xs-12">
				<h1>Our supporters</h1>
			</header>
			<section class="middle row">
				<div class="column md-4 sm-6 xs-6">
					<div>
						<img src="assets/svg/hillel-logo.svg" alt="">
					</div>
				</div>
				<div class="column md-4 sm-6 xs-6">
					<div>
						<img src="assets/svg/ooi-logo.svg" alt="">
					</div>
				</div>
			</section>
	</article>

</div>


<?php include 'footer.php' ?>